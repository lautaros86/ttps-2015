import {NgModule} from '@angular/core';
import {CoreRoutingModule} from './core-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {HeaderComponent} from './shell/header/header.component';
import {ContentComponent} from './shell/content/content.component';
import {FooterComponent} from './shell/footer/footer.component';
import {ShellComponent} from './shell/shell.component';

import {AlertComponent} from '../components';
import {HomeComponent} from '../home/home.component';
import {LoginComponent} from '../login/login.component';
import {RegisterComponent} from '../register/register.component';
import {fakeBackendProvider} from '../helpers/fake-backend';
import {JwtInterceptor} from '../helpers/jwt.interceptor';
import {ErrorInterceptor} from '../helpers/error.interceptor';
import {BrowserModule} from '@angular/platform-browser';
import {AdminComponent} from '../admin/admin.component';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    CoreRoutingModule
  ],
  declarations: [
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    ShellComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AdminComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    fakeBackendProvider
  ],
  exports: [
    ShellComponent
  ]
})
export class CoreModule {
}
