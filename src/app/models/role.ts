export enum Role {
  Admin = 'ADMIN',
  Owner = 'OWNER',
  User = 'USER',
}
